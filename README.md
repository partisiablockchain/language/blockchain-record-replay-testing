# blockchain-record-replay-testing

A record/replay framework that enables testing against a live blockchain. 
The framework records all requests and responses sent through a `BlockchainClient`,
these requests and responses is then used to replay the responses for each request, sent to the live 
chain in future test.

The requests sent in a test, must match the recorded, so the response that is replayed, to guarantee
that the behavior of the chain, would be equivalent, to the recorded response.

The recording client wraps another web-client, where all request and responses, sent and received 
are stored in the recording client. The recording client also stores the time stamps requested 
during execution. At shutdown the recording client writes the request and responses in a JSON file
named `request-response.json` and the time stamps in a JSON file named `now.json`. These files are
written to a given `reference-folder`. 

The replaying client reads the files in the `reference-folder`, and replays them in the order that
they were recorded. 

## Usage Example
````java
final class ExampleTest {

  // Flag to switch to turn recording on for a test run.
  private static final boolean RECORDING = false;

  // Target the testnet in this example.
  private static final String URL = "https://node1.testnet.partisiablockchain.com";

  void setup(TestInfo info) {
    // Determine the reference folder to save recordings in.
    String classFolders = info.getTestClass().get().getName().replace(".", "/");
    String referenceFolder =
        "src/test/resources/reference-tests/" + classFolders + "/" + info.getDisplayName();

    // Create the BlockchainRecordReplay giving recording flag, the referencefolder to write/read 
    // the recording to/from, the chain endpoint to target and the number of shards that chain has.
    BlockchainRecordReplay recordReplay =
        new BlockchainRecordReplay(RECORDING, referenceFolder, URL, 3);
    // Get a BlockchainClient using the record/replay client.
    BlockchainClient blockchainClient = recordReplay.getBlockchainClient();
    // Get a BlockchainTransactionClient using the record/replay client.
    BlockchainTransactionClient transactionClient =
        recordReplay.getTransactionClient(SenderAuthenticationKeyPair.fromString(PRIVATE_KEY));
    
    // ... Setup, where the BlockchainClient and BlockchainTransactionClient are used.
  }
}

````