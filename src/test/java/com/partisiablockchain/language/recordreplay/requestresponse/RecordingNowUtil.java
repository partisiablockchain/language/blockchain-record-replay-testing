package com.partisiablockchain.language.recordreplay.requestresponse;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** Utility class for accessing recorded timestamps. */
public final class RecordingNowUtil {

  private final RecordingNow recordingNow;

  /**
   * Create new {@link RecordingNowUtil}.
   *
   * @param recordingNow the recordings
   */
  public RecordingNowUtil(RecordingNow recordingNow) {
    this.recordingNow = recordingNow;
  }

  /**
   * Get the ith recording.
   *
   * @param i the recording to get
   * @return the recording
   */
  public Long getRecordingNow(int i) {
    return recordingNow.timestamps.get(i);
  }

  /**
   * Get the number of recordings.
   *
   * @return the number of recordings
   */
  public int getNumberOfRecordings() {
    return recordingNow.timestamps.size();
  }
}
