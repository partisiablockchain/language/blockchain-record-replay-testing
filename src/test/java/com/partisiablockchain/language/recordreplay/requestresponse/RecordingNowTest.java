package com.partisiablockchain.language.recordreplay.requestresponse;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.File;
import java.nio.file.Path;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class RecordingNowTest {

  RecordingNow timeSupport;

  @BeforeEach
  void setup() {
    timeSupport = new RecordingNow(Path.of("target/timesupport.json"));
  }

  /** Adding a time, is added to the map of nows, and the counter is increased. */
  @Test
  void addTimeToRecording() {
    timeSupport.add(1234L);
    Assertions.assertThat(timeSupport.timestamps.get(0)).isEqualTo(1234L);
    Assertions.assertThat(timeSupport.timestamps.size()).isEqualTo(1);
    Assertions.assertThat(timeSupport.index).isEqualTo(1);
  }

  /** Multiple times that are added, are all recorded. * */
  @Test
  void multipleCallsToNow() {
    timeSupport.add(1234L);
    timeSupport.add(12345L);
    timeSupport.add(123455L);
    timeSupport.add(123456L);
    timeSupport.add(123457L);

    Assertions.assertThat(timeSupport.timestamps.size()).isEqualTo(5);
    Assertions.assertThat(timeSupport.index).isEqualTo(5);
  }

  /** The shutdown hook for writing at the end of execution is added to the runtime. */
  @Test
  void addShutdownHook() {
    Path referenceFile = Path.of("target/addShutdown/now.json");
    Thread thread = new Thread(() -> timeSupport.writeToFile(referenceFile));
    timeSupport.addWriteAtShutdown(thread);
    Assertions.assertThat(Runtime.getRuntime().removeShutdownHook(thread)).isTrue();
  }

  /** A created time support has a registered shutdown hook. */
  @Test
  void removeShutdownHook() {
    Assertions.assertThat(timeSupport.shutdownWrite).isNotNull();
    Assertions.assertThat(Runtime.getRuntime().removeShutdownHook(timeSupport.shutdownWrite))
        .isTrue();
  }

  /** Write the recorded nows to the given path. */
  @Test
  void writeNows() {
    Path referenceFile = Path.of("target/new-timesupport.json");
    Assertions.assertThat(referenceFile.toFile()).doesNotExist();
    timeSupport.add(10);
    timeSupport.add(11);
    timeSupport.writeToFile(referenceFile);
    Assertions.assertThat(referenceFile.toFile().exists()).isTrue();
    referenceFile.toFile().deleteOnExit();
  }

  /** If the directory and file does not exist, create the dir structure and the file. */
  @Test
  void createFileAndDirIfNotCreated() {
    Path referenceFile = Path.of("target/test-dir/new-timesupport.json");
    File parentDir = referenceFile.toFile().getParentFile();
    if (parentDir.exists()) {
      parentDir.delete();
    }
    Assertions.assertThat(referenceFile.toFile()).doesNotExist();
    Assertions.assertThat(parentDir).doesNotExist();
    timeSupport.add(10);
    timeSupport.add(11);
    timeSupport.writeToFile(referenceFile);
    Assertions.assertThat(referenceFile.toFile().exists()).isTrue();
    referenceFile.toFile().deleteOnExit();
  }
}
