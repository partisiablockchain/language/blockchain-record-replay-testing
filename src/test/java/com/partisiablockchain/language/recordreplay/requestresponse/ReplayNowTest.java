package com.partisiablockchain.language.recordreplay.requestresponse;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.nio.file.Path;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Replaying timestamps from a recording. */
final class ReplayNowTest {

  private ReplayNow replayer;

  @BeforeEach
  void setup() {
    replayer = new ReplayNow(Path.of("src/test/resources/timesupport/timesupport.json"));
  }

  /** If file does not exist the timestamp map is empty. */
  @Test
  public void fileDoesNotExist() {
    ReplayNow replayer = new ReplayNow(Path.of("unknown"));
    Assertions.assertThat(replayer.timestamps.size()).isEqualTo(0);
  }

  /** Get the next recorded timestamp as a replay. */
  @Test
  void getNextRecordedTime() {
    Assertions.assertThat(replayer.index).isEqualTo(0);
    Assertions.assertThat(replayer.timestamps.size()).isEqualTo(5);

    long next = replayer.getNext();

    Assertions.assertThat(next).isEqualTo(1234);
    Assertions.assertThat(replayer.index).isEqualTo(1);
  }

  /** The replayer cannot provide more timestamps then the recorded file contains. */
  @Test
  void cannotGetMoreTimestampsThanRecorded() {
    Assertions.assertThat(replayer.timestamps.size()).isEqualTo(5);
    replayer.getNext();
    replayer.getNext();
    replayer.getNext();
    replayer.getNext();
    replayer.getNext();

    Assertions.assertThatThrownBy(() -> replayer.getNext())
        .hasMessageContaining(
            "The replayer of timestamps does not contain anymore recorded timestamps.");
  }
}
