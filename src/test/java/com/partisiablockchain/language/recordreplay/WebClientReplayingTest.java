package com.partisiablockchain.language.recordreplay;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.client.web.JerseyWebClient;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.dto.ChainId;
import com.partisiablockchain.dto.IncomingTransaction;
import com.partisiablockchain.dto.TransactionPointer;
import com.partisiablockchain.dto.traversal.AvlKeyType;
import com.partisiablockchain.dto.traversal.AvlTraverse;
import com.partisiablockchain.dto.traversal.FieldTraverse;
import com.partisiablockchain.dto.traversal.TraversePath;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import com.secata.tools.rest.testing.client.ClientTestFilter;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.GenericType;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

/** Tests the {@link BlockchainRecordReplay} replaying. */
final class WebClientReplayingTest {

  private JerseyWebClient webClientReplaying;
  private static final String baseUrl = "http://localhost";
  private static final boolean RECORDING = false;

  @BeforeEach
  void setup(TestInfo info) {
    Client jakartaClient = ClientBuilder.newClient();
    webClientReplaying =
        new JerseyWebClient(
            ClientTestFilter.recordReplay(jakartaClient, getRecordSavePath(info), false));
  }

  /** Replay a GET-request retrieving the chain ID. */
  @Test
  public void replayGetChainId() {
    ChainId expected = new ChainId("Id");
    String url = finalizeUrl(BlockchainClient.CHAIN_ID_GET);
    ChainId chainId = webClientReplaying.get(url, ChainId.class);
    ChainId chainIdNoQueries = webClientReplaying.get(url, Map.of(), ChainId.class, Map.of());
    Assertions.assertThat(expected).isEqualTo(chainId);
    Assertions.assertThat(expected).isEqualTo(chainIdNoQueries);
  }

  /** Replay a GET-request with generics retrieving a contracts state avl tree values. */
  @Test
  public void replayGetAvlTreeValues() {
    List<Map.Entry<byte[], byte[]>> expected =
        List.of(
            Map.entry(new byte[] {1}, new byte[] {2}), Map.entry(new byte[] {3}, new byte[] {4}));
    String url = finalizeUrl(BlockchainClient.CONTRACT_STATE_AVL_GET_NEXT_N);
    List<Map.Entry<byte[], byte[]>> gotten =
        webClientReplaying.get(
            url,
            Map.of("n", 2),
            new GenericType<>() {},
            Map.of("address", TestValues.CONTRACT_ADDRESS.writeAsString(), "treeId", 1, "key", 2));
    Assertions.assertThat(gotten.get(0).getValue()).isEqualTo(expected.get(0).getValue());
    Assertions.assertThat(gotten.get(0).getKey()).isEqualTo(expected.get(0).getKey());
    Assertions.assertThat(gotten.get(1).getValue()).isEqualTo(expected.get(1).getValue());
    Assertions.assertThat(gotten.get(1).getKey()).isEqualTo(expected.get(1).getKey());
  }

  /** Replay GET-requests that threw an exception during recording. */
  @Test
  public void replayGetExceptions() {
    Assertions.assertThatThrownBy(() -> webClientReplaying.get(baseUrl, String.class))
        .hasMessage("HTTP 500 Internal Server Error");
    Assertions.assertThatThrownBy(
            () -> webClientReplaying.get(baseUrl, Map.of(), String.class, Map.of()))
        .hasMessage("HTTP 500 Internal Server Error");
  }

  /** Replay a PUT-request with a {@link IncomingTransaction}. */
  @Test
  public void replayPutTransaction() {
    TransactionPointer pointer = new TransactionPointer(TestValues.EMPTY, "Shard0");
    String url = finalizeUrl(BlockchainClient.TRANSACTION_PUT);

    final IncomingTransaction payload = new IncomingTransaction(new byte[10]);
    TransactionPointer put = webClientReplaying.put(url, payload, TransactionPointer.class);
    webClientReplaying.put(url + "/response", payload);
    Assertions.assertThat(put).isEqualTo(pointer);
  }

  /** Replay PUT-requests that threw an exception during recording. */
  @Test
  public void replayPutExceptions() {
    Assertions.assertThatThrownBy(() -> webClientReplaying.put(baseUrl, "value"))
        .hasMessage("500: Internal Server Error");
    Assertions.assertThatThrownBy(() -> webClientReplaying.put(baseUrl, "value", String.class))
        .hasMessage("HTTP 500 Internal Server Error");
  }

  /** Replays a POST-request to the account plugin local state. */
  @Test
  public void replayPostAccountPluginLocalAccount() throws JsonProcessingException {
    String url = finalizeUrl(BlockchainClient.TRAVERSE_ACCOUNT_LOCAL);
    SmallAccountPluginLocalState expected = getDummyLocalAccountPluginState(100);
    TraversePath traversePayload =
        new TraversePath(
            List.of(
                new FieldTraverse("accounts"),
                new AvlTraverse(
                    AvlKeyType.BLOCKCHAIN_ADDRESS,
                    TestValues.FIRST_ACCOUNT_ADDRESS.writeAsString())));
    JsonNode postResponse = webClientReplaying.post(url, traversePayload, JsonNode.class);
    SmallAccountPluginLocalState mappedResponse =
        TestValues.MAPPER.treeToValue(postResponse, SmallAccountPluginLocalState.class);
    Assertions.assertThat(expected).usingRecursiveComparison().isEqualTo(mappedResponse);
  }

  /** Replays a POST-request to the account plugin local state with templates. */
  @Test
  public void replayPostAccountPluginWithQueryParams() throws JsonProcessingException {
    String url = finalizeUrl(BlockchainClient.TRAVERSE_ACCOUNT_LOCAL);
    SmallAccountPluginLocalState expected = getDummyLocalAccountPluginState(80);
    TraversePath traversePayload =
        new TraversePath(
            List.of(
                new FieldTraverse("accounts"),
                new AvlTraverse(
                    AvlKeyType.BLOCKCHAIN_ADDRESS,
                    TestValues.FIRST_ACCOUNT_ADDRESS.writeAsString())));
    JsonNode postResponse =
        webClientReplaying.post(url, traversePayload, JsonNode.class, Map.of("template", 10));
    SmallAccountPluginLocalState mappedResponse =
        TestValues.MAPPER.treeToValue(postResponse, SmallAccountPluginLocalState.class);
    Assertions.assertThat(expected).usingRecursiveComparison().isEqualTo(mappedResponse);
  }

  static String finalizeUrl(String path) {
    return baseUrl + path;
  }

  /** Create a new blockchain transaction client using a replaying blockchain client. */
  @Test
  void createBlockchainTransactionClient() {
    BlockchainRecordReplay blockchainRecordReplay =
        new BlockchainRecordReplay(RECORDING, "src/test/resources/recordreplay", "TESTNET", 3);

    Assertions.assertThat(blockchainRecordReplay).isNotNull();
    BlockchainTransactionClient client =
        blockchainRecordReplay.getTransactionClient(
            new SenderAuthenticationKeyPair(new KeyPair(BigInteger.TEN)));
    Assertions.assertThat(client).isNotNull();
  }

  private static Path getRecordSavePath(TestInfo info) {
    return Path.of("src/test/resources/" + info.getTestMethod().get().getName());
  }

  /**
   * Creates a small local account plugin state.
   *
   * @param mpcTokens amount of mpc tokens in state
   * @return local account plugin state
   */
  static SmallAccountPluginLocalState getDummyLocalAccountPluginState(long mpcTokens) {
    return new SmallAccountPluginLocalState(
        FixedList.create(List.of(new SmallAccountPluginLocalState.Coin(Unsigned256.ONE))),
        AvlTree.create(
            Map.of(
                TestValues.SECOND_ACCOUNT_ADDRESS,
                new SmallAccountPluginLocalState.StakesFromOthers(10, 20))),
        mpcTokens);
  }
}
