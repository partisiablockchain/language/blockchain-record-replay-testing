package com.partisiablockchain.language.recordreplay;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.api.transactionclient.utils.ApiClient;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.RecordingTimeSupport;
import com.partisiablockchain.client.transaction.ReplayTimeSupport;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.client.web.JerseyWebClient;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.tools.rest.RestResources;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import java.math.BigInteger;
import java.util.concurrent.TimeUnit;
import org.assertj.core.api.Assertions;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.jupiter.api.Test;

final class WebClientRecordReplayTest {

  private static final String referenceFolder = "src/test/resources/recordreplay";

  /** If replaying then both the WebClient and the TimeSupport is also the replaying type. */
  @Test
  void getReplayingBlockchainClient() {
    BlockchainRecordReplay blockchainRecordReplay =
        new BlockchainRecordReplay(false, referenceFolder, "chain", 3);
    Assertions.assertThat(blockchainRecordReplay.webClient).isInstanceOf(JerseyWebClient.class);
    Assertions.assertThat(blockchainRecordReplay.timeInfiltrator.getTimeSupport())
        .isInstanceOf(ReplayTimeSupport.class);
  }

  /** If recording then both the WebClient and the TimeSupport is also the recording type. */
  @Test
  void getRecordingBlockchainClient() {
    BlockchainRecordReplay blockchainRecordReplay =
        new BlockchainRecordReplay(
            true,
            referenceFolder,
            "chain",
            3,
            ClientBuilder.newBuilder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .withConfig(new ResourceConfig().register(RestResources.DEFAULT))
                .build());
    Assertions.assertThat(blockchainRecordReplay.webClient).isInstanceOf(JerseyWebClient.class);
    Assertions.assertThat(blockchainRecordReplay.timeInfiltrator.getTimeSupport())
        .isInstanceOf(RecordingTimeSupport.class);
  }

  /** The http client used can be fetched. */
  @Test
  void getClient() {

    BlockchainRecordReplay blockchainRecordReplay =
        new BlockchainRecordReplay(false, referenceFolder, "chain", 3);
    Client client = blockchainRecordReplay.getHttpClient();

    Assertions.assertThat(client).isNotNull();
  }

  /** The web client used can be fetched. */
  @Test
  void getWebClient() {

    BlockchainRecordReplay blockchainRecordReplay =
        new BlockchainRecordReplay(false, referenceFolder, "chain", 3);
    WebClient webClient = blockchainRecordReplay.getWebClient();

    Assertions.assertThat(webClient).isNotNull();
  }

  /** The blockchain client is not created before it is called to get. */
  @Test
  void getBlockchainClient() {
    BlockchainRecordReplay blockchainRecordReplay =
        new BlockchainRecordReplay(false, referenceFolder, "chain", 3);

    Assertions.assertThat(blockchainRecordReplay.blockchainClient).isNull();

    BlockchainClient chain = blockchainRecordReplay.getBlockchainClient();

    Assertions.assertThat(chain).isNotNull();
    Assertions.assertThat(blockchainRecordReplay.blockchainClient).isNotNull();
  }

  /**
   * The transaction client is not created before it is called to get, in the call the underlying
   * blockchain client is also created.
   */
  @Test
  void getTransactionClient() {

    BlockchainRecordReplay blockchainRecordReplay =
        new BlockchainRecordReplay(false, referenceFolder, "chain", 3);

    Assertions.assertThat(blockchainRecordReplay.blockchainClient).isNull();

    BlockchainTransactionClient transactionClient =
        blockchainRecordReplay.getTransactionClient(
            new SenderAuthenticationKeyPair(new KeyPair(BigInteger.TEN)));

    Assertions.assertThat(transactionClient).isNotNull();
    Assertions.assertThat(blockchainRecordReplay.blockchainClient).isNotNull();
  }

  /** The blockchain client is the same everytime it is called after the first time. */
  @Test
  void getBlockchainClientMultipleTimes() {

    BlockchainRecordReplay blockchainRecordReplay =
        new BlockchainRecordReplay(false, referenceFolder, "chain", 3);

    Assertions.assertThat(blockchainRecordReplay.blockchainClient).isNull();

    BlockchainClient expected = blockchainRecordReplay.getBlockchainClient();
    BlockchainClient actual = blockchainRecordReplay.getBlockchainClient();

    Assertions.assertThat(expected).isEqualTo(actual);
  }

  /** A new transaction client is created, when a new key is used. */
  @Test
  void getTransactionClientMultipleTimes() {

    BlockchainRecordReplay blockchainRecordReplay =
        new BlockchainRecordReplay(false, referenceFolder, "chain", 3);

    BlockchainTransactionClient expected =
        blockchainRecordReplay.getTransactionClient(
            new SenderAuthenticationKeyPair(new KeyPair(BigInteger.TEN)));
    BlockchainTransactionClient actual =
        blockchainRecordReplay.getTransactionClient(
            new SenderAuthenticationKeyPair(new KeyPair(BigInteger.ONE)));

    Assertions.assertThat(actual).isNotEqualTo(expected);
  }

  /** Create a Blockchain transaction client with a given private key. */
  @Test
  void getTransactionClientWithSpecificPrivateKey() {

    BlockchainRecordReplay blockchainRecordReplay =
        new BlockchainRecordReplay(false, referenceFolder, "chain", 3);

    Assertions.assertThat(blockchainRecordReplay.blockchainClient).isNull();

    BlockchainTransactionClient transactionClient =
        blockchainRecordReplay.getTransactionClient(BigInteger.TEN);

    Assertions.assertThat(transactionClient).isNotNull();
    Assertions.assertThat(blockchainRecordReplay.blockchainClient).isNotNull();
  }

  /** Create a blockchain client with a specific target and number of shards. */
  @Test
  void getBlockchainClientWithSpecifiedTargetChain() {

    BlockchainRecordReplay blockchainRecordReplay =
        new BlockchainRecordReplay(false, referenceFolder, "chain", 3);
    BlockchainClient specificClient = blockchainRecordReplay.getBlockchainClient("OtherChain", 2);
    BlockchainClient defaultClient = blockchainRecordReplay.getBlockchainClient();

    Assertions.assertThat(specificClient).isNotEqualTo(defaultClient);
    Assertions.assertThat(specificClient).isNotNull();
  }

  /** Create a blockchain transaction client with a specific target and number of shards. */
  @Test
  void getBlockchainTransactionClientWithSpecifiedTargetChain() {

    BlockchainRecordReplay blockchainRecordReplay =
        new BlockchainRecordReplay(false, referenceFolder, "chain", 3);
    BlockchainTransactionClient specificClient =
        blockchainRecordReplay.getTransactionClient(
            new SenderAuthenticationKeyPair(new KeyPair(BigInteger.TEN)), "OtherChain", 2);
    BlockchainTransactionClient defaultClient =
        blockchainRecordReplay.getTransactionClient(
            new SenderAuthenticationKeyPair(new KeyPair(BigInteger.TEN)));

    Assertions.assertThat(specificClient).isNotEqualTo(defaultClient);
    Assertions.assertThat(specificClient).isNotNull();
  }

  /**
   * Create a blockchain transaction client with a specific transaction validity duration and
   * spawned event timeout.
   */
  @Test
  void getBlockchainTransactionClientWithValidityDurationAndSpawnedEventTimeout() {

    BlockchainRecordReplay blockchainRecordReplay =
        new BlockchainRecordReplay(false, referenceFolder, "chain", 3);
    BlockchainTransactionClient specificClient =
        blockchainRecordReplay.getTransactionClient(
            new SenderAuthenticationKeyPair(new KeyPair(BigInteger.TEN)),
            "OtherChain",
            2,
            199999,
            101011);
    BlockchainTransactionClient defaultClient =
        blockchainRecordReplay.getTransactionClient(
            new SenderAuthenticationKeyPair(new KeyPair(BigInteger.TEN)));

    Assertions.assertThat(specificClient).isNotEqualTo(defaultClient);
    Assertions.assertThat(specificClient).isNotNull();
  }

  /**
   * getApiClient creates an ApiClient with the target chain and http client from the
   * BlockchainRecordReplay instance.
   */
  @Test
  void getApiClient() {
    BlockchainRecordReplay blockchainRecordReplay =
        new BlockchainRecordReplay(false, referenceFolder, "chain", 3);
    ApiClient apiClient = blockchainRecordReplay.getApiClient();
    Assertions.assertThat(apiClient.getBasePath()).isEqualTo("chain");
    Assertions.assertThat(apiClient.getHttpClient()).isEqualTo(blockchainRecordReplay.client);
  }

  /**
   * getApiClient creates an ApiClient with the inputted target chain and http client from the
   * BlockchainRecordReplay instance.
   */
  @Test
  void getApiClientWithTargetChain() {
    BlockchainRecordReplay blockchainRecordReplay =
        new BlockchainRecordReplay(false, referenceFolder, "chain", 3);
    ApiClient apiClient = blockchainRecordReplay.getApiClient("otherChain");
    Assertions.assertThat(apiClient.getBasePath()).isEqualTo("otherChain");
    Assertions.assertThat(apiClient.getHttpClient()).isEqualTo(blockchainRecordReplay.client);
  }
}
