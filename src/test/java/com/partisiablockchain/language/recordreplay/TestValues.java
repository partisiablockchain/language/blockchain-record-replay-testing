package com.partisiablockchain.language.recordreplay;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateObjectMapper;

/** Test values. */
public final class TestValues {
  public static final BlockchainAddress FIRST_ACCOUNT_ADDRESS =
      BlockchainAddress.fromString("00a44d16e47871a20cdc8b2beb6bb2fe16d1ba8ff1");

  public static final BlockchainAddress SECOND_ACCOUNT_ADDRESS =
      BlockchainAddress.fromString("00c4b91b81531fb571f0a815346b808cfd266b4fbf");

  public static final BlockchainAddress CONTRACT_ADDRESS =
      BlockchainAddress.fromString("02c14c29b2697f3c983ada0ee7fac83f8a937e2ecd");

  public static final Hash EMPTY = Hash.create(s -> {});

  public static final ObjectMapper MAPPER = StateObjectMapper.createObjectMapper();
}
