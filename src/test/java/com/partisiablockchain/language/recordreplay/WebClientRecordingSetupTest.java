package com.partisiablockchain.language.recordreplay;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.File;
import java.nio.file.Path;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

/** Test of the recording webclient. */
final class WebClientRecordingSetupTest {

  /** If the reference folder already exists use it for recording. */
  @Test
  void preexistingFolderForReferenceFiles(TestInfo info) {
    String referenceFolderString = getReferenceFolder(info);
    Path referenceFolderPath = Path.of(referenceFolderString);

    File referenceDir = referenceFolderPath.toFile();
    if (!referenceDir.exists()) {
      referenceDir.mkdirs();
    }

    Assertions.assertThat(referenceDir.isDirectory()).isTrue();
  }

  private static String getReferenceFolder(TestInfo info) {
    String recordFile = "target/" + info.getTestMethod().get().getName();
    return recordFile;
  }
}
