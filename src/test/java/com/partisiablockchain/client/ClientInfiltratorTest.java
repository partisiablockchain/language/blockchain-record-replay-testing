package com.partisiablockchain.client;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.client.web.JerseyWebClient;
import jakarta.ws.rs.client.ClientBuilder;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Creating a Blockchain client using the infiltrator. */
final class ClientInfiltratorTest {

  /** Creating a client with the infiltrator creates a new BlockchainClient. */
  @Test
  void creationOfClient() {
    BlockchainClient client =
        ClientInfiltrator.blockchainClient("", 3, new JerseyWebClient(ClientBuilder.newClient()));

    Assertions.assertThat(client).isNotNull();
  }
}
