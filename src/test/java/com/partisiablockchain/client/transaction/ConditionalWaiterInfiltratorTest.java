package com.partisiablockchain.client.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test the infiltrator. */
final class ConditionalWaiterInfiltratorTest {

  /** Create an infiltrator and get the conditional waiter and the time support. */
  @Test
  void createInfiltrator() {
    ConditionalWaiterInfiltrator infiltrator =
        new ConditionalWaiterInfiltrator(ConditionWaiterImpl.TimeSupport.SYSTEM);
    ConditionWaiter forTest = infiltrator.getConditionalWaiter();

    ConditionWaiterImpl.TimeSupport timeSupport = infiltrator.getTimeSupport();

    Assertions.assertThat(forTest).isNotNull();
    Assertions.assertThat(timeSupport).isNotNull();
  }
}
