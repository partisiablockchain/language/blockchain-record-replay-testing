package com.partisiablockchain.client.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.recordreplay.requestresponse.ReplayNow;
import com.partisiablockchain.language.recordreplay.requestresponse.ReplayingNowUtil;
import java.nio.file.Path;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test the replaying of timestamps. */
final class ReplayTimeSupportTest {

  private ReplayingNowUtil replayingNowUtil;
  private ReplayTimeSupport replayingNow;

  @BeforeEach
  void setup() {
    ReplayNow replayNow = new ReplayNow(Path.of("src/test/resources/timesupport/timesupport.json"));
    replayingNowUtil = new ReplayingNowUtil(replayNow);
    replayingNow = new ReplayTimeSupport(replayNow);
  }

  /** Replaying timestamps provides the recorded timestamps. */
  @Test
  void getNowsFromRecording() {
    Assertions.assertThat(replayingNowUtil.getNumberOfRecordings()).isEqualTo(5);
    Assertions.assertThat(replayingNow.now()).isEqualTo(1234L);
    Assertions.assertThat(replayingNow.now()).isEqualTo(12345L);
    Assertions.assertThat(replayingNow.now()).isEqualTo(123455L);
    Assertions.assertThat(replayingNow.now()).isEqualTo(123456L);
    Assertions.assertThat(replayingNow.now()).isEqualTo(123457L);
    Assertions.assertThat(replayingNowUtil.getIndex()).isEqualTo(5);
  }

  /** Sleeping is not available when replaying. */
  @Test
  void sleepingDoesNothing() {
    Assertions.assertThat(replayingNowUtil.getIndex()).isEqualTo(0);
    replayingNow.sleep(10000);
    Assertions.assertThat(replayingNowUtil.getIndex()).isEqualTo(0);
  }
}
