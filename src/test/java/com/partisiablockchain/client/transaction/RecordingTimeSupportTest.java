package com.partisiablockchain.client.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.recordreplay.requestresponse.RecordingNow;
import com.partisiablockchain.language.recordreplay.requestresponse.RecordingNowUtil;
import java.nio.file.Path;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Testing the recording of timestamps. */
final class RecordingTimeSupportTest {

  RecordingNowUtil recordingNowUtil;
  private RecordingTimeSupport recordingTimeSupport;

  @BeforeEach
  void setup() {
    RecordingNow recordingNow = new RecordingNow(Path.of("target/timesupport"));
    recordingNowUtil = new RecordingNowUtil(recordingNow);
    recordingTimeSupport =
        new RecordingTimeSupport(recordingNow, ConditionWaiterImpl.TimeSupport.SYSTEM);
  }

  /** Get now is added to the recorded now map. */
  @Test
  void getNowIsAddedToRecording() {
    long now = recordingTimeSupport.now();
    Assertions.assertThat(recordingNowUtil.getRecordingNow(0)).isEqualTo(now);
    now = recordingTimeSupport.now();
    Assertions.assertThat(recordingNowUtil.getRecordingNow(1)).isEqualTo(now);
  }
}
