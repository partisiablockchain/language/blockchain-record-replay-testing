package com.partisiablockchain.client.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.recordreplay.requestresponse.RecordingNow;
import com.secata.tools.coverage.ExceptionConverter;
import java.nio.file.Path;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test the sleep call to the system, is propagated. */
public final class RecordingTimeSleepTest {

  /** When recording, the time support is called in the sleep call. */
  @Test
  void callSleep() {
    RecordingNow recordingNow = new RecordingNow(Path.of("target/timesupport"));
    TestTimeSupport timeSupport = new TestTimeSupport();
    RecordingTimeSupport recordingTimeSupport = new RecordingTimeSupport(recordingNow, timeSupport);
    ExceptionConverter.run(() -> recordingTimeSupport.sleep(10));
    Assertions.assertThat(timeSupport.sleepCalled).isTrue();
  }

  /** Testing time support to inject for check function calls. */
  static final class TestTimeSupport implements ConditionWaiterImpl.TimeSupport {

    boolean sleepCalled;

    TestTimeSupport() {
      sleepCalled = false;
    }

    @Override
    public long now() {
      return 0;
    }

    @Override
    public void sleep(long ms) throws Exception {
      sleepCalled = true;
    }
  }
}
