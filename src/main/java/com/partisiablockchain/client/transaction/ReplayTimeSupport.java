package com.partisiablockchain.client.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.recordreplay.ConstantsRecordReplay.JSON;
import static com.partisiablockchain.language.recordreplay.ConstantsRecordReplay.NOW;

import com.partisiablockchain.language.recordreplay.requestresponse.ReplayNow;
import java.nio.file.Paths;

/** The replaying of calls for current time now. */
public final class ReplayTimeSupport implements ConditionWaiterImpl.TimeSupport {

  private final ReplayNow nows;

  /**
   * Creates a new time support for testing.
   *
   * @param directory the directory the recording is placed in.
   */
  public ReplayTimeSupport(String directory) {
    this(new ReplayNow(Paths.get(directory + NOW + JSON)));
  }

  ReplayTimeSupport(ReplayNow replayNow) {
    this.nows = replayNow;
  }

  @Override
  public long now() {
    return nows.getNext();
  }

  @Override
  public void sleep(long ms) {}
}
