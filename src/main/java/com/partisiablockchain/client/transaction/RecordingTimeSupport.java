package com.partisiablockchain.client.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.recordreplay.ConstantsRecordReplay.JSON;
import static com.partisiablockchain.language.recordreplay.ConstantsRecordReplay.NOW;

import com.partisiablockchain.language.recordreplay.requestresponse.RecordingNow;
import java.nio.file.Path;

/**
 * A {@link ConditionWaiterImpl.TimeSupport} that uses the underlying system to provide the current
 * time and for sleeping. It records the first call to now, as this is expected to be same call when
 * testing something that signs a transaction.
 */
public final class RecordingTimeSupport implements ConditionWaiterImpl.TimeSupport {

  final RecordingNow nows;
  private final ConditionWaiterImpl.TimeSupport timeSupport;

  /**
   * Records all calls to now, using the underlying system.
   *
   * @param workingDirectory The directory to record the first call to now.
   */
  public RecordingTimeSupport(String workingDirectory) {
    this(
        new RecordingNow(Path.of(workingDirectory + NOW + JSON)),
        ConditionWaiterImpl.TimeSupport.SYSTEM);
  }

  RecordingTimeSupport(RecordingNow recordingNow, ConditionWaiterImpl.TimeSupport system) {
    this.timeSupport = system;
    this.nows = recordingNow;
  }

  @Override
  public long now() {
    long now = timeSupport.now();
    nows.add(now);
    return now;
  }

  @Override
  public void sleep(long ms) throws Exception {
    timeSupport.sleep(ms);
  }
}
