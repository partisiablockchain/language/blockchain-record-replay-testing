package com.partisiablockchain.client.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.client.BlockchainContractClient;

/** Dependency infiltrator to create a blockchain transaction client for tests. */
public final class BlockchainTransactionClientInfiltrator {

  private BlockchainTransactionClientInfiltrator() {}

  /**
   * Create a blockchain transaction client to use during a record/replay test.
   *
   * @param blockchainClient the blockchain client used to send transactions.
   * @param authentication the sender authentication to sign transactions with.
   * @param timeSupport the time provider to get the current time from.
   * @param conditionWaiter the waiter.
   * @param transactionValidityDuration the duration for a transaction to valid from current time.
   * @param spawnedEventTimeout the timeout for waiting for events to execute.
   * @return the created transaction client.
   */
  public static BlockchainTransactionClient createForTest(
      BlockchainContractClient blockchainClient,
      SenderAuthentication authentication,
      ConditionWaiterImpl.TimeSupport timeSupport,
      ConditionWaiter conditionWaiter,
      long transactionValidityDuration,
      long spawnedEventTimeout) {
    return BlockchainTransactionClient.createForTest(
        blockchainClient,
        authentication,
        timeSupport::now,
        conditionWaiter,
        transactionValidityDuration,
        spawnedEventTimeout);
  }
}
