package com.partisiablockchain.client.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** Dependency infiltrator to create a conditional waiter for tests. */
public final class ConditionalWaiterInfiltrator {

  private final ConditionWaiterImpl.TimeSupport timeSupport;

  /**
   * Create a conditional waiter to use during tests.
   *
   * @param timeSupport the time support to pass to conditional waiter.
   */
  public ConditionalWaiterInfiltrator(ConditionWaiterImpl.TimeSupport timeSupport) {
    this.timeSupport = timeSupport;
  }

  /**
   * Get the ConditionWaiter with the underlying time support.
   *
   * @return the condition waiter.
   */
  public ConditionWaiter getConditionalWaiter() {
    return ConditionWaiterImpl.createForTest(timeSupport);
  }

  /**
   * Get the time support.
   *
   * @return the timesupport.
   */
  public ConditionWaiterImpl.TimeSupport getTimeSupport() {
    return timeSupport;
  }
}
