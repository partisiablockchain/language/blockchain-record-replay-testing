package com.partisiablockchain.client;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.client.web.WebClient;

/** Circumvents package-only scope of core/client. */
public final class ClientInfiltrator {

  private ClientInfiltrator() {}

  /**
   * Creates a new blockchain client with a supplied web-client.
   *
   * @param baseUrl The base of the web of the targeted PBC.
   * @param numberOfShards The number of shards at the targeted PBC.
   * @param webClient A web client that communicates via HTTP.
   * @return a newly created blockchain client.
   */
  public static BlockchainClient blockchainClient(
      String baseUrl, int numberOfShards, WebClient webClient) {
    return new BlockchainClient(baseUrl, numberOfShards, webClient);
  }
}
