package com.partisiablockchain.language.recordreplay;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.api.transactionclient.utils.ApiClient;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.ClientInfiltrator;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.BlockchainTransactionClientInfiltrator;
import com.partisiablockchain.client.transaction.ConditionalWaiterInfiltrator;
import com.partisiablockchain.client.transaction.RecordingTimeSupport;
import com.partisiablockchain.client.transaction.ReplayTimeSupport;
import com.partisiablockchain.client.transaction.SenderAuthentication;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.client.web.JerseyWebClient;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.tools.rest.RestResources;
import com.secata.tools.rest.testing.client.ClientTestFilter;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.concurrent.TimeUnit;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * Create a BlockchainClient and BlockchainTransactionClient using a record/replay client, to use
 * for testing applications and programs against a live blockchain.
 */
public final class BlockchainRecordReplay {

  private final String targetChain;
  private final int numberOfShards;
  final Client client;
  BlockchainClient blockchainClient;
  final JerseyWebClient webClient;
  final ConditionalWaiterInfiltrator timeInfiltrator;

  /**
   * Create a new record replay client.
   *
   * @param recording is the client provided recording.
   * @param referenceFolder the folder to read/save recording from/to.
   * @param targetChain the chain to interact with during test run.
   * @param numberOfShards the number of shards running on chain.
   */
  public BlockchainRecordReplay(
      boolean recording, String referenceFolder, String targetChain, int numberOfShards) {
    this(
        recording,
        referenceFolder,
        targetChain,
        numberOfShards,
        ClientTestFilter.recordReplay(createClient(), Path.of(referenceFolder), recording));
  }

  /**
   * Create a new record replay client using a given client.
   *
   * @param recording is the client provided recording.
   * @param referenceFolder the folder to read/save recording from/to.
   * @param targetChain the chain to interact with during test run.
   * @param numberOfShards the number of shards running on chain.
   * @param client the http client used.
   */
  public BlockchainRecordReplay(
      boolean recording,
      String referenceFolder,
      String targetChain,
      int numberOfShards,
      Client client) {
    this.targetChain = targetChain;
    this.numberOfShards = numberOfShards;
    this.client = client;
    this.webClient = new JerseyWebClient(client);

    if (recording) {
      this.timeInfiltrator =
          new ConditionalWaiterInfiltrator(new RecordingTimeSupport(referenceFolder));
    } else {
      this.timeInfiltrator =
          new ConditionalWaiterInfiltrator(new ReplayTimeSupport(referenceFolder));
    }
  }

  /**
   * Get the underlying http client.
   *
   * @return The client.
   */
  public Client getHttpClient() {
    return client;
  }

  /**
   * Get the created webclient.
   *
   * @return the webclient.
   */
  public WebClient getWebClient() {
    return webClient;
  }

  /**
   * Create a blockchain client for requests and responses.
   *
   * @return a newly created blockchain client.
   */
  public BlockchainClient getBlockchainClient() {
    if (blockchainClient == null) {
      blockchainClient = ClientInfiltrator.blockchainClient(targetChain, numberOfShards, webClient);
    }
    return blockchainClient;
  }

  /**
   * Create a blockchain client for requests and responses.
   *
   * @param targetChain the chain to interact with during test run.
   * @param numberOfShards the number of shards running on chain.
   * @return a newly created blockchain client.
   */
  public BlockchainClient getBlockchainClient(String targetChain, int numberOfShards) {
    return ClientInfiltrator.blockchainClient(targetChain, numberOfShards, webClient);
  }

  /**
   * Create a transaction client.
   *
   * @param sender the authentication for the sender, used when signing transactions.
   * @return the transaction client.
   */
  public BlockchainTransactionClient getTransactionClient(SenderAuthentication sender) {

    return BlockchainTransactionClientInfiltrator.createForTest(
        getBlockchainClient(),
        sender,
        timeInfiltrator.getTimeSupport(),
        timeInfiltrator.getConditionalWaiter(),
        BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
        BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);
  }

  /**
   * Create a transaction client with a specific private key.
   *
   * @param privateKey the private key, to use for signing transactions.
   * @return the transaction client.
   */
  public BlockchainTransactionClient getTransactionClient(BigInteger privateKey) {

    return BlockchainTransactionClientInfiltrator.createForTest(
        getBlockchainClient(),
        new SenderAuthenticationKeyPair(new KeyPair(privateKey)),
        timeInfiltrator.getTimeSupport(),
        timeInfiltrator.getConditionalWaiter(),
        BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
        BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);
  }

  /**
   * Create a transaction client.
   *
   * @param sender the authentication for the sender, used when signing transactions.
   * @param targetChain the chain to interact with during test run.
   * @param numberOfShards the number of shards running on chain.
   * @return the transaction client.
   */
  public BlockchainTransactionClient getTransactionClient(
      SenderAuthentication sender, String targetChain, int numberOfShards) {

    return BlockchainTransactionClientInfiltrator.createForTest(
        getBlockchainClient(targetChain, numberOfShards),
        sender,
        timeInfiltrator.getTimeSupport(),
        timeInfiltrator.getConditionalWaiter(),
        BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
        BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);
  }

  /**
   * Create a transaction client.
   *
   * @param sender the authentication for the sender, used when signing transactions.
   * @param targetChain the chain to interact with during test run.
   * @param numberOfShards the number of shards running on chain.
   * @param transactionValidityDuration how long a signed transaction is valid for inclusion in a
   *     block.
   * @param spawnedEventTimeout time for a spawned event to be included in a block before timing
   *     out.
   * @return the transaction client.
   */
  public BlockchainTransactionClient getTransactionClient(
      SenderAuthentication sender,
      String targetChain,
      int numberOfShards,
      long transactionValidityDuration,
      long spawnedEventTimeout) {

    return BlockchainTransactionClientInfiltrator.createForTest(
        getBlockchainClient(targetChain, numberOfShards),
        sender,
        timeInfiltrator.getTimeSupport(),
        timeInfiltrator.getConditionalWaiter(),
        transactionValidityDuration,
        spawnedEventTimeout);
  }

  /**
   * Create an ApiClient for the target chain using the record-replay client. Can be used to create
   * a ChainController or ShardController.
   *
   * @return the ApiClient
   */
  public ApiClient getApiClient() {
    return getApiClient(targetChain);
  }

  /**
   * Create an ApiClient for the target chain using the record-replay client. Can be used to create
   * a ChainController or ShardController.
   *
   * @param targetChain the chain to interact with during test run.
   * @return the ApiClient
   */
  public ApiClient getApiClient(String targetChain) {
    ApiClient apiClient = new ApiClient();
    apiClient.setHttpClient(client);
    apiClient.setBasePath(targetChain);
    return apiClient;
  }

  private static Client createClient() {
    Client jakartaClient =
        ClientBuilder.newBuilder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .withConfig(new ResourceConfig().register(RestResources.DEFAULT))
            .build();
    return jakartaClient;
  }
}
