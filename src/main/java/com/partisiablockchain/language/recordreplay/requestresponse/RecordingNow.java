package com.partisiablockchain.language.recordreplay.requestresponse;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

/** Record all calls for current time, so replaying produces results with the same time used. */
public final class RecordingNow {

  final Thread shutdownWrite;
  Map<Integer, Long> timestamps;
  int index;

  /**
   * Create a current time now recorder, which writes all the recorded nows to the file given at
   * shutdown.
   *
   * @param jsonFile the file to write the recording to.
   */
  public RecordingNow(Path jsonFile) {
    this.timestamps = new HashMap<>();
    this.index = 0;
    shutdownWrite = new Thread(() -> this.writeToFile(jsonFile));
    addWriteAtShutdown(shutdownWrite);
  }

  /**
   * Add the shutdown hook to run the given thread. R
   *
   * @param thread the thread to run at shutdown.
   */
  void addWriteAtShutdown(Thread thread) {
    Runtime.getRuntime().addShutdownHook(thread);
  }

  /** Write the current recorded timestamps at the location given to location. */
  void writeToFile(Path jsonFile) {
    ExceptionConverter.run(
        () -> {
          File writeToFile = jsonFile.toFile();
          if (!writeToFile.exists()) {
            writeToFile.getParentFile().mkdirs();
            writeToFile.createNewFile();
          }
          new ObjectMapper().writerWithDefaultPrettyPrinter().writeValue(writeToFile, timestamps);
        });
  }

  /**
   * Add a now time to the recordings.
   *
   * @param now the now time to add.
   */
  public void add(long now) {
    timestamps.put(index, now);
    index++;
  }
}
