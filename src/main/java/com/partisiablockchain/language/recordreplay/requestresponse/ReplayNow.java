package com.partisiablockchain.language.recordreplay.requestresponse;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.secata.tools.coverage.ExceptionConverter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

/** Replay calls for current time, read from the given file. */
public final class ReplayNow {

  Map<Integer, Long> timestamps;
  int index;

  /**
   * Create a current time now replayer, reading the now time recorded in the file.
   *
   * @param jsonFile the file to read the now times from.
   */
  public ReplayNow(Path jsonFile) {
    if (Files.exists(jsonFile)) {
      this.timestamps =
          ExceptionConverter.call(
              () -> new ObjectMapper().readValue(jsonFile.toFile(), new TypeReference<>() {}));
    } else {
      this.timestamps = new HashMap<>();
    }
    this.index = 0;
  }

  /**
   * Get the next now to use.
   *
   * @return the next now.
   */
  public long getNext() {

    if (!timestamps.containsKey(index)) {
      throw new RuntimeException(
          "The replayer of timestamps does not contain anymore recorded timestamps.");
    }
    long now = timestamps.get(index);
    index++;
    return now;
  }
}
